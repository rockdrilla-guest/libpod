####> This option file is used in:
####>   podman manifest push, push
####> If file is edited, make sure the changes
####> are applicable to all of those.
#### **--compression-format**=**gzip** | *zstd*

Specifies the compression format to use.  Supported values are: `gzip`, and `zstd`.  The default is `gzip` unless overridden in the containers.conf file.
